json.username @agent.username
json.nickname @agent.nickname
if @agent.current_sign_in_at.present?
  json.current_sign_in_at default_log_time(@agent.current_sign_in_at)
end
json.current_sign_in_ip @agent.current_sign_in_ip
json.points default_currency(@agent.points)
unless @agent.parent.nil?
  json.parent_points default_currency(@agent.parent.points)
end