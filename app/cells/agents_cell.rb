class AgentsCell < BaseCell
  helper ReportHelper

  def filter_bar(args)
    @value = args[:value]
    render
  end

  def information(args)
    @agent = args[:agent]
    render
  end

  # agent row
  # cache :list do |options|
  #   [ options[:agent], I18n.locale ]
  # end
  def list(args)
    @agent = args[:agent]
    render
  end

  def finances(args)
    @cashout_stats = args[:cashout_stats]
    @agent = args[:agent]
    render
  end

  def withdraws(args)
    @withdraw_stats = args[:withdraw_stats]
    @agent = args[:agent]
    render
  end
end
