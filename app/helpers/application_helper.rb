module ApplicationHelper
  # 在當頁nav link加上active class
  def nav_link(link_text, link_path, opt = {})
    class_name = current_page?(link_path) ? ' active' : ''
    if opt[:class].nil?
      opt[:class] ||= class_name
    else
      opt[:class] += class_name
    end
    content_tag(:li, opt) do
      link_to link_text, link_path
    end
  end

  def current_group?(controller_group)
    controller = params[:controller].split('/')
    controller[0] == controller_group
  end

  # 遊戲設定遊戲款項
  def game_setting_paths
    game_settings = { "slot_machines" => game_slot_machines_path }
    game_settings['racing_games'] = game_racing_games_path unless Setting.api_site? || true
    unless Setting.shop_site?
      game_settings['little_mary_machines'] = game_little_mary_machines_path
      game_settings['poker_machines'] = game_poker_machines_path
      game_settings['roulette_machines'] = game_roulette_machines_path
      game_settings['fishing_joy_machines'] = game_fishing_joy_machines_path
    end

    game_settings
  end

  def game_list_kind(game)
    list = case game
    when '2'
      ['111', '112', '113', '114', '115', '211', '311', '411', '511', '611'].collect{|list| [t('platform.east.game_kind.' + list), list]}
    when '3'
      ['3001', '3002', '3003', '3005', '3006', '3007', '3008', '3010', '3011', '3012', '3014', '3015'].collect{|list| [t('platform.bbin.game_kind.' + list), list]}
    when '4'
      ['BAC', 'CBAC', 'LINK', 'DT', 'SHB', 'ROU', 'FT', 'LBAC', 'ULPK', 'SBAC'].collect{|list| [t('platform.ag.game_kind.' + list), list]}
    end
    list
  end

  def update_platform_time(type)
    return '' if type == 'all'
    type = type == 'mg' || type == 'bbin' || type.nil? ? 'ag' : type
    default_log_time(GamePlatform.find_by_name(type).updated_at)
  end

  def sort_link(column, title = nil)
    title ||= column.titleize
    direction = column == sort_column && sort_direction == "desc" ? "asc" : "desc"
    icon = sort_direction == "asc" ? "glyphicon glyphicon-chevron-up" : "glyphicon glyphicon-chevron-down"
    icon = column == sort_column ? icon : ""
    link_to "#{title} <span class='#{icon}'></span>".html_safe, {column: column, direction: direction}
  end

  def default_zero(value, key)
    value.nil? ? 0 : value[key]
  end
end
