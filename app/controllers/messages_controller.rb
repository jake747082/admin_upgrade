class MessagesController < ApplicationController
  before_action :set_breadcrumbs_path
  before_action :set_messages, only: [:edit, :update, :destroy]
  autocomplete :user, :username
  autocomplete :agent, :username

  def index
    @user_groups_mapping = Hash[UserGroup.pluck(:id, :name)]
    if params[:query].present?
      users = User.where("username LIKE ? OR nickname LIKE ?", "%#{params[:query]}%", "%#{params[:query]}%")
      agents = Agent.where("username LIKE ? OR nickname LIKE ?", "%#{params[:query]}%", "%#{params[:query]}%")
    end
    @messages = Message.includes(:user, :agent).search(users, agents).page(params[:page])
  end

  def new
    drop_breadcrumb(t('breadcrumbs.message.new'))
    @message_form = MessageForm::Create.new(Message.new)
  end

  def create
    @message_form = MessageForm::Create.new(Message.new)
    if @message_form.create(messages_params)
      # save log
      current_admin.create_log(:message, current_admin, remote_ip, id: @message_form.id)
      redirect_to messages_path, notice: t('flash.shared.create.finish')
    else
      render :new
    end
  end

  def edit
    drop_breadcrumb(t('breadcrumbs.message.edit'))
    @message_form = MessageForm::Create.new(@message)
    @users = @message.read_messages.joins(:user).select('read_messages.*, users.*')
  end

  def update
    @message_form = MessageForm::Update.new(@message)
    if @message_form.update(messages_params)
      # save log
      current_admin.create_log(:message_update, current_admin, remote_ip, id: @message.id)
      redirect_to messages_path, notice: t('flash.shared.update.finish')
    else
      render :edit, notice: t('flash.shared.update.fail')
    end
  end

  def destroy
    if @message.update(deleted: true)
      current_admin.create_log(:message_delete, current_admin, remote_ip, id: @message.id)
      redirect_to messages_path, notice: t('flash.message.delete.finish')
    else
      redirect_to messages_path, notice: t('flash.message.delete.fail')
    end
  end

  protected

  def set_breadcrumbs_path
    drop_breadcrumb(t('breadcrumbs.messages'), messages_path)
  end

  def set_messages
    @message = Message.find(params[:id])
  end

  def messages_params
    # params.require(:message).permit(:title, :content, :agent_username, :username, :groups)
    params.require(:message).permit(:title, :content, :username, groups: [])
  end

  def valid_permission!
    authorize :message, :all?
  end
end