class Agent::LocksController < Agent::BaseController
  before_action :set_agent
  before_action :set_locking_service, only: [:create, :destroy]

  # POST /agents/:agent_id/locks
  def create
    if @agent_lock_service.lock { |resource| resource.create_log(:lock, current_admin, remote_ip) }
      redirect_to edit_agent_path(@agent), notice: t('flash.agent.locks.finish', total_agents_count: @agent.total_agents_count, total_users_count: @agent.total_users_count)
    else
      redirect_to edit_agent_path(@agent), alert: t('falsh.agent.error')
    end
  end

  # GET /agents/:agent_id/locks/destroy_confirm.js
  def destroy_confirm; end

  # GET /agents/:agent_id/locks/parent_lock_notice.js
  def parent_lock_notice; end

  # DELETE /agents/:agent_id/locks
  def destroy
    callback_proc = Proc.new { |unlock_target| unlock_target.create_log(:unlock, current_admin, remote_ip) }
    operation_method = if params[:tree] then :unlock_all else :unlock end

    if @agent_lock_service.send(operation_method, &callback_proc)
      redirect_to edit_agent_path(@agent), notice: success_notice_message
    else
      redirect_to edit_agent_path(@agent), alert: t('falsh.agent.error')
    end
  end

  protected

  def set_locking_service
    @agent_lock_service = Agent::TreeLock.new(@agent)
  end

  def success_notice_message
    if params[:tree] || @agent.in_final_level?
      t('flash.agent.unlock_tree.finish', total_agents_count: @agent.total_agents_count, total_users_count: @agent.total_users_count)
    else
      t('flash.agent.unlock.finish')
    end
  end
end
