class Agent::FinancesController < Agent::BaseController
  include DateRangeFilter
  before_action :set_default_date_range, :set_agent

  def index
    drop_breadcrumb(t('breadcrumbs.finances'), finances_path)
    drop_breadcrumb(@agent.title)
    @lobby_id = params[:lobby_id]
    @agent = params[:agent].nil? || params[:agent].empty? ? @agent : Agent.find(params[:agent])
    render 'finances/index'
  end

  protected
  
  def valid_permission!
    authorize :finances, :all?
  end
end
