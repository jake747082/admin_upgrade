class Agent::UserPermissionsController < Agent::BaseController
  before_action :set_agent, :set_service

  def create
    callback_proc = Proc.new { |unlock_target| unlock_target.create_log(:user_permission_open, current_admin, remote_ip) }
    operation_method = if params[:tree] then :open_all! else :open! end

    if @service.send(operation_method, &callback_proc)
      redirect_to agents_path, notice: t('flash.shared.update.finish')
    else
      redirect_to agents_path, alert: t('falsh.agent.error')
    end
  end

  def open_confirm; end

  # 用來提醒上層需開啟權限
  def notice; end

  def destroy
    if @service.close! { |resource| resource.create_log(:user_permission_close, current_admin, remote_ip) }
      redirect_to agents_path, notice: t('flash.shared.update.finish')
    else
      redirect_to agents_path, alert: t('falsh.agent.error')
    end
  end

  protected

  def set_service
    @service = Agent::ControlUserPermission.new(@agent)
  end

  def success_notice_message
    if params[:tree] || @agent.in_final_level?
      t('flash.agent.open_tree.finish', total_agents_count: @agent.total_agents_count)
    else
      t('flash.agent.open.finish')
    end
  end
end