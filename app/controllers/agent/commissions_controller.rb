class Agent::CommissionsController < Agent::BaseController
  before_action :set_agent
  before_action :setup_form_data, only: [:users, :details]

  def index
    @agents = Agent.cashes.list_agent.search(params[:query]).page(params[:page])
  end
  
  def show
    drop_breadcrumb(t('breadcrumbs.agent.cash_types'), agent_cash_types_path)
    drop_breadcrumb(@agent.title)
    
    @commission_reports = Finance::AgentCommissionView.call(@agent, {})

    @game_rate = Reports::AgentCommission.game_rate
  end

  def users
    effective_bet_users = ThirdPartyBetForm.effective_bet_users(@agent.id, "#{@begin_date} 00:00:00", "#{@end_date} 23:59:59").pluck(:user_id)
    @users = User.search_users(effective_bet_users).page(params[:page])
  end

  def details
    @sum = {deposit: {credit_actual: 0, credit: 0}, cashout: {credit_actual: 0, credit: 0}}
    @details = Withdraw.list_by_type(params[:type]).where(updated_at: @begin_date..@end_date).where(agent_id: @agent.id).page(params[:page])
    @sum[:deposit][:credit_actual] = Withdraw.list_by_type(params[:type]).where(updated_at: @begin_date..@end_date).where(agent_id: @agent.id).where(cash_type_cd: 0).sum(:credit_actual)
    @sum[:deposit][:credit] = Withdraw.list_by_type(params[:type]).where(updated_at: @begin_date..@end_date).where(agent_id: @agent.id).where(cash_type_cd: 0).sum(:credit)
    @sum[:cashout][:credit_actual] = Withdraw.list_by_type(params[:type]).where(updated_at: @begin_date..@end_date).where(agent_id: @agent.id).where(cash_type_cd: 1).sum(:credit_actual)
    @sum[:cashout][:credit] = Withdraw.list_by_type(params[:type]).where(updated_at: @begin_date..@end_date).where(agent_id: @agent.id).where(cash_type_cd: 1).sum(:credit)
    render "/reports/users/details"
  end

  def update
    if @agent.update(commission_view: true)
      @agent.create_log(:commission_view_open, current_admin, remote_ip)
      redirect_to :back, notice: t('flash.shared.update.finish')
    else
      redirect_to :back
    end
  end
  
  def destroy
    if @agent.commission_at.nil? || Date.today.at_beginning_of_month != @agent.commission_at.to_date.at_beginning_of_month
      @commission_reports = Finance::AgentCommissionView.call(@agent, {})
      if @commission_reports.nil?
        redirect_to :back, notice: t('flash.shared.update.fail')
      else
        amount = 0
        # default
        begin_date ||= @commission_reports[0][:begin_date].to_date
        end_date ||= @commission_reports[0][:end_date].to_date
        @commission_reports.each do |report|
          # begin 取最早; end 取最晚
          begin_date = report[:begin_date].to_date if begin_date > report[:begin_date]
          end_date = report[:end_date].to_date if end_date < report[:end_date]
          amount += report[:commission_sum].to_f
        end
        if @agent.update(commission_at: Time.now, commission_view: false)
          # 佣金發送紀錄
          @agent.agent_commission_logs.new(begin_date: begin_date, end_date: end_date, amount: amount).save!
          
          @agent.create_log(:commission_finish, current_admin, remote_ip)
          redirect_to :back, notice: t('flash.shared.update.finish')
        else
          redirect_to :back
        end
      end
    else
      # 若還沒到當月底不可結算
      redirect_to :back, flash: { error: t('flash.agent.commission.finish_date.failed')}
    end
  end

  protected

  def setup_form_data
    @begin_date = params[:begin_date]
    @end_date = params[:end_date]
  end

end