class Agent::BanksController < Agent::BaseController
  before_action :set_agent, :set_bank

  # GET /agent/bank/edit
  def edit
    drop_breadcrumb(@agent.username)
    @bank = BankForm::Update.new(@agent.bank_account)
  end

  # PUT /agent/bank
  def update
    @bank = BankForm::Update.new(@agent.bank_account)
    if @bank.update(bank_params)
      @agent.create_log(:update_bank, current_admin, remote_ip)
      redirect_to edit_agent_banks_path(@agent), notice: t('flash.shared.update.finish', name: @agent.username)
    else
      render :edit
    end
  end

  protected

  def valid_permission!
    authorize :agent, :all?
  end

  def set_bank
    @bank = @agent.bank_account
    redirect_to edit_agent_url(@agent) if @agent.blank?
  end

  def bank_params
    params.require(:bank_account).permit(:bank_id, :province, :subbranch, :city, :account, :security_code, :security_code_confirmation)
  end
end