class Agent::MachinesController < Agent::BaseController
  before_action :set_agent, :set_breadcrumbs_path

  # GET /agents/1/machines
  def index
    drop_breadcrumb(t('breadcrumbs.machine.root'))
    @machines = @agent.all_machines.includes(:agent).page(params[:page])
    render 'machines/index'
  end

  protected

  def set_breadcrumbs_path
    drop_breadcrumb(@agent.title, agent_machines_path(@agent))
  end

  def valid_permission!
    super and authorize :machine, :all?
  end
end