class CashFlow::BankAccountsController < ApplicationController
  require 'bigdecimal'
  before_action :valid_permission!, :set_breadcrumbs_path

  def index
    @category = params[:category] || ""
    @accounts = AdminBankAccount.all.page(params[:page]) if @category == "all" || @category == ""
    @accounts = AdminBankAccount.where(deposit: true).page(params[:page]) if @category == "deposit"
    @accounts = AdminBankAccount.where(deposit: false).page(params[:page]) if @category == "withdraw"
  end

  def new
    @account = AdminBankAccount.new
    drop_breadcrumb(t('breadcrumbs.admin.new_bank_account'), new_cash_flow_bank_account_path)
  end

  def create
    @account = AdminBankAccount.new(account_params)
    if @account.save
      current_admin.create_log(:create_bank_account, current_admin, remote_ip, account: @account.account)
      redirect_to cash_flow_bank_accounts_path, notice: t('flash.shared.create.finish')
    else
      render :new
    end
  end

  def edit
    @account = AdminBankAccount.find(params[:id])
    drop_breadcrumb(t('breadcrumbs.admin.edit_bank_account'), edit_cash_flow_bank_account_path(@account))
  end

  def update
    @account = AdminBankAccount.find(params[:id])
    if @account.update(account_params)
      current_admin.create_log(:update_bank_account, current_admin, remote_ip, account: @account.account)
      redirect_to cash_flow_bank_accounts_path, notice: t('flash.shared.update.finish')
    else
      render :edit
    end
  end

  def edit_cumulative_amount
    @account = AdminBankAccount.find(params[:id])
    action = params[:amount_action]
    cumulative_amount = BigDecimal.new(@account.cumulative_amount.to_s)
    action_amount = BigDecimal.new(params[:action_amount].to_s)
    edit_amount = BigDecimal.new(params[:edit_amount].to_s)
    final_amount = cumulative_amount + action_amount if action == "in"
    final_amount = (cumulative_amount - action_amount).round(2) if action == "out"
    final_amount = edit_amount if edit_amount != cumulative_amount

    if @account.update(cumulative_amount: final_amount.to_f)
      @account.increment!(:update_count, by = 1 )
      current_admin.create_bank_account_log(:update_cumulative_amount, current_admin, remote_ip, {account: @account.account, cumulative_amount: final_amount.to_f}, @account)
      redirect_to cash_flow_bank_accounts_path, notice: t('flash.shared.update.finish')
    else
      redirect_to cash_flow_bank_accounts_path, alert: t('flash.bank_account.update.fail')
    end
  end

  def cumulative_amount
    @account = AdminBankAccount.find(params[:account])
    respond_to do |format|
      format.js
    end
  end

  # 銀行帳戶 累積金額 紀錄
  def operate_histories
    account = params[:account]
    @logs = PublicActivity::Activity.where(recipient_id: account, key: 'admin.update_cumulative_amount')
    respond_to do |format|
      format.js
    end
  end

  protected

  def set_breadcrumbs_path
    drop_breadcrumb(t('breadcrumbs.admin.bank_account'), cash_flow_bank_accounts_path)
  end

  def account_params
    params.require(:admin_bank_account).permit(:bank_id, :sub_bank_name, :title, :account, :cumulative_amount, :limit_update_count, :update_count, :deposit, :open)
  end

  def valid_permission!
    authorize :withdraw, :cash_flow?
  end
end
