module System
  class SiteConfigsController < BaseController
    before_action :set_config, :set_breadcrumbs_path

    # GET /site_config/edit
    def edit; end

    # PATCH/PUT /site_config
    def update
      if @config.update_and_notice_each_platform!(update_config_params)
        redirect_to edit_system_site_config_url, notice: t('flash.site_configs.update.finish')
      else
        render :edit
      end
    end

    protected

    def set_config
      @config = site_config
    end

    def set_breadcrumbs_path
      drop_breadcrumb(t('breadcrumbs.site_configs'), system_site_config_path)
    end
    
    def update_config_params
      params.require(:site_config).permit(:lobby_maintenance, :agent_maintenance, :lobby_marquee, :agent_marquee, :cash_maintenance, :cash_marquee,:base_jp,:jp_rate,:jp_reset)
    end
  end
end