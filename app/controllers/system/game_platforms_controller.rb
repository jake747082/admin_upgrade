class System::GamePlatformsController < System::BaseController
  before_action :set_breadcrumbs_path

  def edit
    @config = GamePlatform.where.not(name: 'mdragon')
  end
  
  def update
    respond_to do |format|
      format.js {
        if params[:id].to_i > 0
          @platform = GamePlatform.find(params[:id])
          from_status = @platform.status.to_s
          case params[:status].to_i
            when 0
              @status = @platform.update!(status: :close)
            when 1
              @status = @platform.update!(status: :open)
            when 2
              @status = @platform.update!(status: :maintain)
            else
              @status = false
            end
        else
          @status = false
        end
        current_admin.create_log(:game_platform_config, current_admin, remote_ip, game_platform: @platform.name, from_status: from_status, to_status: @platform.status.to_s, update_status: @status)
      }
    end
  end

  private

  def set_breadcrumbs_path
    drop_breadcrumb(t('breadcrumbs.setting.game_platform'), edit_system_game_platform_path)
  end
end