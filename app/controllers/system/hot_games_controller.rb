module System
    class HotGamesController < BaseController
      before_action :set_hot_game, :set_hot_games_path

      def edit
        @option = PlatformGame.where(game_type: ["slot","fish"]).order(:code)
      end

      def update
        if @hot_game.update!(update_games_params)
          redirect_to edit_system_hot_game_path, notice: t('flash.site_configs.update.finish')
        else
          render :edit
        end
      end
  
      protected
  
      def set_hot_game
        @hot_game = HotGame.first_or_create
      end
  
      def set_hot_games_path
        drop_breadcrumb(t('breadcrumbs.setting.hot_games'), edit_system_hot_game_path)
      end
      
      def update_games_params
        params.require(:hot_game).permit(:game1_id,:game2_id,:game3_id,:game4_id,:game5_id,:game6_id,:game7_id,:game8_id,:game9_id,:game10_id)
      end
    end
  end