class NewsController < ApplicationController
  before_action :valid_permission!
  before_action :set_news, only: [:edit, :update, :destroy]
  before_action :set_breadcrumbs_path, only: [:index, :new, :edit]
  before_action :set_news_breadcrumbs_path, only: [:list, :detail]

  def index
    @news = New.where(search_params).page(params[:page])
  end

  def new
    @news = New.new
  end

  def create
    @news = New.new(news_params)
    if @news.save
      # save log
      current_admin.create_log(:news, current_admin, remote_ip, id: @news.id)
      redirect_to news_index_path, notice: t('flash.shared.create.finish')
    else
      render :new
    end
  end

  def edit; end

  def update
    if @news.update(news_params)
      current_admin.create_log(:news_update, current_admin, remote_ip, id: @news.id)
      redirect_to news_index_path, notice: t('flash.shared.update.finish')
    else
      render :edit
    end
  end

  def destroy
    if @news.update(deleted: true)
      current_admin.create_log(:news_delete, current_admin, remote_ip, id: @news.id)
      redirect_to news_index_path, notice: t('flash.news.delete.finish')
    else
      redirect_to news_index_path, notice: t('flash.news.delete.fail')
    end
  end

  def list
    @news = New.admin_list.page(params[:page])
  end

  def detail
    @news = New.admin_list.find(params[:news_id])
    drop_breadcrumb(@news.title)
  end
  
  protected
  
  # 最新公告
  def set_news_breadcrumbs_path
    drop_breadcrumb(t('breadcrumbs.news'), list_news_index_path)
  end

  def set_breadcrumbs_path
    drop_breadcrumb(t('breadcrumbs.news_management'), news_index_path)
  end

  def valid_permission!
    authorize :news, :all?
  end

  def set_news
    @news = New.find(params[:id])
  end

  def search_params
    params.permit(:admin_port, :agent_port, :www_port)
  end

  def news_params
    params.require(:new).permit(:title, :content, :admin_port, :agent_port, :www_port, :important)
  end
end
