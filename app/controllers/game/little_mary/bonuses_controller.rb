class Game::LittleMary::BonusesController < Game::BaseController
  before_action :set_breadcrumbs_path

  # GET /game/little_mary/bonus
  def index
    @bonuses = LittleMaryBonus.all
  end

  # DELETE /game/little_mary/bonus
  def destroy
    if LittleMaryBonus::Remove.call(params[:id])
      redirect_to :back, notice: t('flash.bonus.delete.finish')
    else
      redirect_to :back, alert: t('flash.bonus.delete.fail')
    end
  end

  private
    def set_breadcrumbs_path
      drop_breadcrumb(t('breadcrumbs.setting.little_mary_machines'), game_little_mary_machines_path)
      drop_breadcrumb(t('breadcrumbs.setting.bonus'), game_little_mary_bonuses_path)
    end
end