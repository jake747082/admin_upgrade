class Game::Slot::JackpotsController < Game::BaseController
  before_action :set_breadcrumbs_path, :set_slot_setting

  # GET /games/slot/jp
  def show; end

  # GET /games/slot/jp/edit
  def edit; end  

  # PUT/PATCH /games/slot/jp
  def update
    @slot_settings.assign_attributes(slot_setting_params)
    push_jackpot_rules_updated = @slot_settings.jackpot_range_changed?

    if @slot_settings.save
      if push_jackpot_rules_updated
        Jackpot::RealtimePush.push_jackpot_setting_changed!(@slot_settings)
      end
      redirect_to game_slot_jackpot_url, notice: t('flash.shared.update.finish')
    else
      render :edit
    end
  end

  private
    def set_breadcrumbs_path
      drop_breadcrumb(t('breadcrumbs.setting.slot_machines'), game_slot_machines_path)
      drop_breadcrumb(t('breadcrumbs.setting.jackpots'), game_slot_jackpot_path)
    end

    def set_slot_setting
      @slot_settings = GameSetting.first
    end

    def slot_setting_params
      params.require(:slot_settings).permit(:jackpot_accumulate_rate,
                                            :jackpot_md_range_begin, :jackpot_md_range_end, :jackpot_sm_range_begin, 
                                            :jackpot_sm_range_end, :jackpot_sm_interval, :jackpot_md_interval)
    end
end