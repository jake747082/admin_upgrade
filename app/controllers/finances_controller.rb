class FinancesController < ApplicationController
  include DateRangeFilter
  before_action :valid_permission!, :set_default_date_range

  def index
    @lobby_id = params[:lobby_id]
    @agent = params[:agent].nil? || params[:agent].empty? ? @agent : Agent.find(params[:agent])
    drop_breadcrumb(t('breadcrumbs.finances'), finances_path)
  end

  def export
    setup_form_data
    if @lobby_id.present?
      # 查詢單一會員資料
      user_id = User.lobby_id_to_id(@lobby_id)
      @user = user_id > 0 ? User.find_by_id(user_id) : nil
      @cashout_stats = Finance::ChildrenCashout.new.get_user(@begin_date, @end_date, @user)
      filename = "#{Setting.title}_#{@lobby_id}(#{@begin_date}_to_#{@end_date}).xlsx"
    else
      username = @agent.nil? ? current_admin.username : @agent.username
      @search_agent = params[:agent].nil? || params[:agent].empty? ? @agent : Agent.find(params[:agent])
      @cashout_stats = Finance::ChildrenCashout.call(@begin_date, @end_date, @search_agent)
      filename = "#{Setting.title}_#{username}(#{@begin_date}_to_#{@end_date}).xlsx"
    end
    if (@agent.present? && @agent.in_final_level?) || @lobby_id.present?
      # 有會員資料的
      @cashout_list = @cashout_stats.player_cashout_list
      respond_to do |format|
        format.xlsx {
          render xlsx: "export", template: 'finances/export', filename: filename
        }
      end
    else
      # 只有代理資料的
      @cashout_list = @cashout_stats.list
      respond_to do |format|
        format.xlsx {
          render xlsx: "export_agent", template: 'finances/export_agent', filename: filename
        }
      end
    end
  end

  protected

  def setup_form_data
    @begin_date = params[:begin_date]
    @end_date = params[:end_date]
    @lobby_id = params[:lobby_id]
    @agent = params[:agent] == nil ? nil : Agent.find(params[:agent])
  end

  def valid_permission!
    authorize :finance, :all?
  end
end