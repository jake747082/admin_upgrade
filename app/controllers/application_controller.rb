class ApplicationController < ActionController::Base
  helper_method :sort_column, :sort_direction

  protect_from_forgery with: :exception
  before_action :authenticate_admin!, :set_base_breadcrumbs

  [ Pundit, ExceptionRenderer, SiteHelper, LangSwitcher ].each { |m| include m }

  layout 'application'

  protected

  alias_method :pundit_user, :current_admin

  # 重新設定 breadcrumbs path
  def set_base_breadcrumbs
    no_breadcrumbs
    drop_breadcrumb(t('breadcrumbs.root'), '/')
  end

  # 導向遊戲設定頁面
  def render_game_page
    redirect_to game_root_url if current_admin.game_manager?
  end

  private

  def sortable_columns
    []
  end

  def sort_column
    sortable_columns.include?(params[:column]) ? params[:column] : "created_at"
  end

  def sort_direction
    %w[asc desc].include?(params[:direction]) ? params[:direction] : "desc"
  end
end
