class Withdraw::AgentDepositsController < Withdraw::BaseController
  before_action :set_withdraw
  autocomplete :agent, :username
  # GET /agent_withdraw/:deposits/new
  def new; end

  def create
    @withdraw_params = withdraw_params
    if @withdraw.deposit
      @withdraw.agent.create_log(:deposit, current_admin, remote_ip ,points: @withdraw.points, agent_withdraw_id: @withdraw.model.id, trade_no: @withdraw.model.trade_no, status: 'transferred')
      unless @withdraw.agent.parent.nil?
        @withdraw.agent.parent.create_log(:deduct, current_admin, remote_ip ,points: @withdraw.points.to_f, agent_withdraw_id: @withdraw.cashout_model.id, trade_no: @withdraw.cashout_model.trade_no, status: 'transferred', related_agent_withdraw_id: @withdraw.model.id)
      end
      redirect_to new_withdraw_agent_deposit_path, notice: t('flash.agent_withdraw.deposits.finish')
    else
      render :new
    end
  end

  def autocomplete_agent_username
    term = params[:term]
    level_cd = params[:level_cd]
    agents = Agent.where('username LIKE ?', "%#{term}%").where(agent_level_cd: level_cd || 0).where(type_cd: 1).order(:username).all
    render :json => agents.map { |agent| {:id => agent.id, :label => agent.username, :value => agent.username} }
  end

  protected

  def set_breadcrumbs
    drop_breadcrumb(t('breadcrumbs.agent_withdraws.deposits'))
  end

  def set_withdraw
    @withdraw_params ||= params[:agent_withdraw]
    @withdraw = AgentWithdrawForm::Deposit.new(@withdraw_params)
  end

  def withdraw_params
    params.require(:agent_withdraw).permit(:username, :points, :note, :admin_note, :level_cd)
  end

  def valid_permission!
    authorize :withdraws, :agent?
  end
end
