class Withdraw::AgentWithdrawsController < Withdraw::BaseController

  before_action :set_withdraw_type
  def index
    @withdraws = AgentWithdraw.includes(:agent).search(params[:query]).withdraw_type(@type).page(params[:page])
  end

  def show
    @withdraw = AgentWithdraw.includes(:agent).find(params[:id])
  end

  protected

  def set_breadcrumbs
    drop_breadcrumb(t("breadcrumbs.withdraws.agent_withdraws"))
  end

  def set_withdraw_type
    @type = params[:type]
  end

  def valid_permission!
    authorize :withdraws, :agent?
  end
end