class Schedules::BaseController < ApplicationController
  before_action :valid_permission!

  protected

  def set_agent_breadcrumbs_path
    drop_breadcrumb(t('breadcrumbs.schedules.racing'), schedules_racings_path)
  end

  def valid_permission!
    authorize :schedule, :all?
  end
end