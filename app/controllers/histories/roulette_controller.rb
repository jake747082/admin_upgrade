class Histories::RouletteController < Histories::BaseController
  before_action :valid_not_shop_site!

  private

  def set_breadcrumb
    drop_breadcrumb(t('breadcrumbs.histories.roulette'))
  end

  def set_bet_forms
    @bet_forms = RouletteBetForm.where(role_search_params).datetime_range(@begin_date, @end_date).includes(:user, :agent, :director, :shareholder)
  end

  def role_search_params
    params[:role].present? ? params.require(:role).permit(:agent_id, :user_id, :shareholder_id, :director_id, :roulette_machine_id) : {}
  end
end
