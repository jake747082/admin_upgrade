class User::Bonuses::SlotsController < User::BaseController
  before_action :set_user, :set_jackpot_info

  # GET /users/1/bonus/slot/new
  def new
    @bonus = @user.bonuses.new
  end

  # POST /user/1/bonus/slot
  def create
    @bonus = @user.bonuses.new(bonus_params)
    if @bonus.save
      redirect_to game_slot_bonuses_url, notice: t('flash.bonus.create.finish')
    else
      render :new
    end
  end

  protected

  def bonus_params
    params.require(:bonus).permit(:bonus_type, :bonus)
  end

  def set_jackpot_info
    @total_jackpot = SlotMachine.total_jackpot
    @game_setting = GameSetting.first!
  end

  def valid_permission!
    authorize :system, :all?
  end
end