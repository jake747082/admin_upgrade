class User::BanksController < User::BaseController
  before_action :set_user, :set_bank

  # GET /account/bank/edit
  def edit
    drop_breadcrumb(@user.system_id)
    @bank = BankForm::Update.new(@user.bank_account)
    # @bank = @user.bank_account
  end

  # PUT /account/bank
  def update
    @bank = BankForm::Update.new(@user.bank_account)
    if @bank.update(bank_params)
      @user.create_log(:update_bank, current_admin, remote_ip)
      redirect_to edit_user_banks_path(@user), notice: t('flash.shared.update.finish', name: @user.system_id)
    else
      render :edit
    end
  end

  protected

  def valid_permission!
    authorize :user, :update?
  end

  def set_bank
    @bank = @user.bank_account
    redirect_to edit_user_url(@user) if @bank.blank?
  end

  def bank_params
    params.require(:bank_account).permit(:bank_id, :province, :subbranch, :city, :account, :security_code, :security_code_confirmation)
  end
end