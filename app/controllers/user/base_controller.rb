class User::BaseController < ApplicationController
  before_action :valid_permission!, :set_user_breadcrumbs_path

  protected

  def set_user_breadcrumbs_path
    drop_breadcrumb(t('breadcrumbs.user.root'), users_path)
  end

  def set_user
    @user = User.find(params[:user_id] || params[:id])
  end

  def valid_permission!
    authorize :user, :all?
  end

  def valid_only_cash_site!
    redirect_to root_url unless Setting.cash_site? && !Setting.api_site?
  end
end