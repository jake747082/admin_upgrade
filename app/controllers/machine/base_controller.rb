class Machine::BaseController < ApplicationController
  before_action :valid_permission!, :valid_shop_site!, :set_machine_breadcrumbs_path

  protected

  def set_machine_breadcrumbs_path
    drop_breadcrumb(t('breadcrumbs.machine.root'), machines_path)
  end

  def only_for_final_level_agent
    redirect_to root_url unless current_agent.in_final_level?
  end

  def set_machine
    @machine = Machine.find(params[:machine_id] || params[:id])
  end

  def valid_permission!
    authorize :machine, :all?
  end

  def valid_shop_site!
    render_404! unless Setting.shop_site?
  end
end