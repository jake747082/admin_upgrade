class Machine::BonusesController < Machine::BaseController
  before_action :set_machine, :set_jackpot_info

  # GET /machines/1/bonus/new.js
  def new
    @bonus = @machine.bonuses.new
  end

  # POST /machine/1/bonus.js
  def create
    @bonus = @machine.bonuses.new(bonus_params)
    if @bonus.save
      redirect_to game_slot_bonuses_url, notice: t('flash.bonus.create.finish')
    else
      render :new
    end
  end

  protected

  def bonus_params
    params.require(:bonus).permit(:bonus_type, :bonus)
  end

  def set_jackpot_info
    @total_jackpot = SlotMachine.total_jackpot
    @game_setting = GameSetting.first!
  end

  def valid_permission!
    authorize :system, :all?
  end
end