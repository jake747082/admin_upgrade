class DashboardController < ApplicationController
  before_action :render_game_page
  # GET /
  def index; end
  # GET /
  def site_map
    respond_to do |format|
      format.js
    end
  end
end
