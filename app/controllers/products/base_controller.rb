class Products::BaseController < ApplicationController
  before_action :valid_permission!, :set_breadcrumbs

  protected

  def valid_permission!
    authorize :withdraws, :all?
  end
end