class SchedulePolicy < ApplicationPolicy
  def all?
    admin.super_manager? && (Setting.shop_site? || Setting.cash_site?) && false
  end
end