class SystemPolicy < ApplicationPolicy

  def all?
    admin.super_manager? || admin.site_manager?
  end
end