class WithdrawPolicy < UserPolicy
  def all?
    (admin.super_manager? || admin.report_manager? || admin.main_manager? || admin.custom_service?) && Setting.cash_site?
  end
  
  # 金流管理
  def cash_flow?
    false
  end

  def agent?
    (admin.super_manager? || admin.report_manager? || admin.custom_service?) && Setting.cash_site?
  end
  # 人工存款
  def deposit?
    !(admin.main_manager? || admin.custom_service?)
  end
  # 人工提款
  def cashout?
    !(admin.main_manager? || admin.custom_service?)
  end
  # 代理買分
  def agent_deposits?
    !(admin.main_manager? || admin.custom_service?)
  end
  # 代理提款
  def agent_cashouts?
    !(admin.main_manager? || admin.custom_service?)
  end
  # 金流紀錄
  def agent_withdraw?
    !(admin.main_manager? || admin.custom_service?)
  end
end