class System::PagePolicy < SystemPolicy
  def all?
    super && Setting.cash_site?
  end
end