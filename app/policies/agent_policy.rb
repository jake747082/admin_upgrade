class AgentPolicy < ApplicationPolicy
  def all?
    admin.super_manager?
  end

  def advanced?
    admin.super_manager?
  end
end