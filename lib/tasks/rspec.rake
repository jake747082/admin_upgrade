namespace :rspec do
  task :create => [:environment, 'parallel:create', 'parallel:prepare']
  task :test => [:environment, 'parallel:spec']
end